package com.example.demo33.jpa;

import com.example.demo33.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car,Integer> {
List<Car> findById(int id);

    List<Car> findByName(String name);
}
