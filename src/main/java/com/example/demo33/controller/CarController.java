package com.example.demo33.controller;

import com.example.demo33.model.Car;
import com.example.demo33.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    @Autowired
    CarService carServiceObj;
    @PostMapping("/addcar")
    public String addCar(@RequestBody Car carObj){
        String str=carServiceObj.addCar(carObj);
        return  str;
    }
    @GetMapping("/getcar")
    public List<Car> fetchCar()
    {
        return carServiceObj.getCar();
    }
    @GetMapping("/Carbyid/{id}")
    public List<Car> getCarById(@PathVariable int id){
        return carServiceObj.getcarbyid(id);
    }
   @GetMapping("/Carbyname/{name}")
    public  List<Car> getCarByName(@PathVariable String name){
        return carServiceObj.getcarbyname(name);
   }
   @DeleteMapping("/deleteByid/{id}")
    public void deleteCar(@PathVariable int id){
         carServiceObj.deletecar(id);
   }



}
