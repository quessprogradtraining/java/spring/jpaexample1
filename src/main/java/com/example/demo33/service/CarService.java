package com.example.demo33.service;

import com.example.demo33.jpa.CarRepository;
import com.example.demo33.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {
    @Autowired
    CarRepository carRepositoryObj;
    public String addCar(Car carObj) {
        carRepositoryObj.save(carObj);
        return "car added";
    }

    public List<Car> getCar() {
        return carRepositoryObj.findAll();
    }

    public List<Car> getcarbyid(int id) {
        return carRepositoryObj.findById(id);
    }

    public List<Car> getcarbyname(String name) {
        return carRepositoryObj.findByName(name);
    }

    public void deletecar(int id) {
        carRepositoryObj.deleteById(id);
    }
}
